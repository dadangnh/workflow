version: "3.4"

services:
  ###> doctrine/doctrine-bundle ###
  database:
    image: postgres:14-alpine
    environment:
      POSTGRES_USER: db_user
      POSTGRES_PASSWORD: db_pass
      POSTGRES_DB: db_name
    ports:
      - "5432:5432"
    restart: unless-stopped
    networks:
      - workflow
    volumes:
      - db_data:/var/lib/postgresql/data:rw
  ###< doctrine/doctrine-bundle ###

  dbmongodb:
    # In production, you may want to use a managed database service
    image: mongo:latest
    environment:
      - MONGO_INITDB_DATABASE=workflow
      - MONGO_INITDB_ROOT_USERNAME=workflow
      # You should definitely change the password in production
      - MONGO_INITDB_ROOT_PASSWORD=workflow
    restart: unless-stopped
    networks:
      - workflow
    volumes:
      - mongo_data:/var/lib/mongodb/data:rw
      # You may use a bind-mounted host directory instead, so that it is harder to accidentally remove the volume and lose all your data!
      # - ./docker/db/data:/var/lib/mongodb/data:rw
    ports:
      - "27017:27017"

  redis:
    image: redis:latest
    ports:
      - "6379:6379"
    restart: unless-stopped
    networks:
      - workflow

  php:
    build:
      context: .
      target: app_php
      args:
        SYMFONY_VERSION: ${SYMFONY_VERSION:-}
        STABILITY: ${STABILITY:-stable}
    restart: unless-stopped
    ports:
      - "9000:9000"
    healthcheck:
      interval: 10s
      timeout: 3s
      retries: 3
      start_period: 30s
    depends_on:
      - redis
      - database
      - dbmongodb
    links:
      - database
      - redis
      - dbmongodb
    networks:
      - workflow
    environment:
      # Run "composer require symfony/orm-pack" to install and configure Doctrine ORM
      DATABASE_URL: postgresql://${POSTGRES_USER:-db_user}:${POSTGRES_PASSWORD:-db_pass}@database:5432/${POSTGRES_DB:-db_name}?serverVersion=${POSTGRES_VERSION:-14}
      # Run "composer require symfony/mercure-bundle" to install and configure the Mercure integration
      MERCURE_URL: ${CADDY_MERCURE_URL:-http://caddy/.well-known/mercure}
      MERCURE_PUBLIC_URL: https://${SERVER_NAME:-localhost}/.well-known/mercure
      MERCURE_JWT_SECRET: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      REDIS_URL: redis://redis:6379
      MONGODB_URL: mongodb://workflow:workflow@db-mongodb
      MONGODB_DB: workflow
      SYMFONY_VERSION:

  caddy:
    build:
      context: .
      target: app_caddy
    depends_on:
      - php
    environment:
      SERVER_NAME: ${SERVER_NAME:-localhost, caddy:80}
      MERCURE_PUBLISHER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      MERCURE_SUBSCRIBER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      PWA_UPSTREAM: ${CADDY_PWA_UPSTREAM:-localhost}
      BACKEND_FASTCGI: ${BACKEND_FASTCGI_CONTAINER_NAME:-php}:${BACKEND_FASTCGI_CONTAINER_PORT:-9000}
    restart: unless-stopped
    networks:
      - workflow
    volumes:
      - caddy_data:/data
      - caddy_config:/config
    ports:
      # HTTP
      - target: 80
        published: ${HTTP_PORT:-80}
        protocol: tcp
      # HTTPS
      - target: 443
        published: ${HTTPS_PORT:-443}
        protocol: tcp
      # HTTP/3
      - target: 443
        published: ${HTTP3_PORT:-443}
        protocol: udp

  nginx:
    build:
      context: .
      target: app_nginx
    depends_on:
      - php
    environment:
      SERVER_NAME: ${SERVER_NAME:-localhost}
      MERCURE_PUBLISHER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      MERCURE_SUBSCRIBER_JWT_KEY: ${CADDY_MERCURE_JWT_SECRET:-!ChangeThisMercureHubJWTSecretKey!}
      PWA_UPSTREAM: ${CADDY_PWA_UPSTREAM:-localhost}
      BACKEND_FASTCGI: ${BACKEND_FASTCGI_CONTAINER_NAME:-php}:${BACKEND_FASTCGI_CONTAINER_PORT:-9000}
    restart: unless-stopped
    links:
      - php
    networks:
      - workflow
    volumes:
      - ./:/srv/app
#      - nginx_data:/data
#      - nginx_config:/config
    ports:
      # HTTP
      - target: 80
        published: ${HTTP_PORT:-80}
        protocol: tcp
      # HTTPS
      - target: 443
        published: ${HTTPS_PORT:-443}
        protocol: tcp
      # HTTP/3
      - target: 443
        published: ${HTTP3_PORT:-443}
        protocol: udp

# Mercure is installed as a Caddy module, prevent the Flex recipe from installing another service
###> symfony/mercure-bundle ###
###< symfony/mercure-bundle ###

volumes:
  php_socket:
  ###> doctrine/doctrine-bundle ###
  db_data:
  ###< doctrine/doctrine-bundle ###
  caddy_data:
  caddy_config:
  mongo_data:
#  nginx_data:
#  nginx_conig:

networks:
  workflow:
    driver: bridge

###> symfony/mercure-bundle ###
###< symfony/mercure-bundle ###
