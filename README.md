# Workflow

[![pipeline status](https://gitlab.com/dadangnh/workflow/badges/master/pipeline.svg)](https://gitlab.com/dadangnh/workflow/-/commits/master)
[![coverage report](https://gitlab.com/dadangnh/workflow/badges/master/coverage.svg)](https://gitlab.com/dadangnh/workflow/-/commits/master)

![Lines of code](https://img.shields.io/tokei/lines/gitlab.com/dadangnh/workflow)

![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/dadangnh/workflow)

Source code service workflow.

## Canonical source

The canonical source of DJP IAM where all development takes place is [hosted on GitLab.com](https://gitlab.com/dadangnh/workflow).

## Installation

Please check [INSTALLATION.md](INSTALLATION.md) for the installation instruction.

## Contributing

This is an open source project, and we are very happy to accept community contributions.

# License

This code is published under [GPLv3 License](LICENSE).
