<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SimpleTest extends ApiTestCase
{
    public function testGetWorkflowCase(): void
    {
        // Expect to get 401 status code because no Token supplied
        try {
            $response = static::createClient()->request('GET', '/workflow_cases');
        } catch (TransportExceptionInterface $e) {
        }
        self::assertResponseStatusCodeSame(401);
    }

    public function testWhoAmI(): void
    {
        // Expect to get 401 status code because no Token supplied
        try {
            $response = static::createClient()->request('POST', '/whoami');
        } catch (TransportExceptionInterface $e) {
        }
        self::assertResponseStatusCodeSame(200);
    }


}
