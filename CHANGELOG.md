# Workflow Changelog

## Version 1.3.0
  * Updated Symfony Components and dependencies (#14)
  * Updated caddy config (#15)
  * Fix Typo on Entity (#16)

## Version 1.2.1
  * Fix Attributes usage on controller (#13)
  * Updated dependencies

## Version 1.2.0
  * Updated docker and infrastructure configuration (#12)
  * Updated symfony components and dependencies
  * Updated symfony recipes

## Version 1.1.1
  * Use php command instead of symfony cli and allow host based auth for service (#9)
  * Use new symfony cli path (#10)
  * Use new docker compose naming standard (#11)
  * Updated symfony components and dependencies
  * Updated symfony recipes

## Version 1.1.0
  * Upgrade Symfony major version to v6.1 (#8)
  * Updated symfony recipes

## Version 1.0.3
  * Allow use GitHub Actions Private Runner (#7)
  * Updated symfony components and dependencies

## Version 1.0.2
  * Updated symfony components and dependencies
  * Updated symfony recipes

## Version 1.0.1
  * Upgrade Shared Auth Library (#5)
  * Fixes GitHub Action Test (#6)
  * Updated symfony components and dependencies

## Version 1.0.0
  * First release. Contains all working code for initial workload
