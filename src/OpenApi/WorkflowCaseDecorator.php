<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;
use ArrayObject;

final class WorkflowCaseDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {}

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['caseOutputRequest'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'start_date' => [
                    'type' => 'string',
                    'example' => '2022-01-01',
                ],
                'end_date' => [
                    'type' => 'string',
                    'example' => '2022-01-10',
                ],
            ],
        ]);

        $schemas['caseOutputResponse'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'code' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'messages' => [
                    'type' => 'object',
                    'readOnly' => true
                ]
            ],
        ]);

        $countCaseFromDateRangeItem = new Model\PathItem(
            ref: 'WorkflowCase',
            post: new Model\Operation(
                operationId: 'ctasCountWorkflowCaseByDateRangeItem',
                tags: ['WorkflowCase'],
                responses: [
                    '200' => [
                        'description' => 'Retrieves workflow cases from a specific range of date',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/caseOutputResponse',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Retrieves workflow cases from a specific range of date',
                requestBody: new Model\RequestBody(
                    description: 'Please provide parameters for the request consist of start_date
                                        and end_date of the case',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/caseOutputRequest',
                            ],
                        ],
                    ]),
                ),
            ),
        );


        $openApi->getPaths()->addPath('/workflow_cases/case_output', $countCaseFromDateRangeItem);

        return $openApi;
    }
}

