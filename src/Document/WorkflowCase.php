<?php

namespace App\Document;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\MongoDbOdm\Filter\SearchFilter;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Repository\WorkflowCaseRepository;

#[MongoDB\Document(
    repositoryClass: WorkflowCaseRepository::class
)]
#[MongoDB\ShardKey(
    keys: [
        'ticketNumber' => 'ASC',
        'employeeId' => 'ASC'
    ]
)]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'security' => 'is_granted("ROLE_USER")',
            'security_message' => 'Only a valid user can access this.'
        ],
        'post' => [
            'security'=>'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            'security_message'=>'Only admin/app can add new resource to this entity type.'
        ]
    ],
    itemOperations: [
        'get' => [
            'security' => 'is_granted("ROLE_USER")',
            'security_message' => 'Only a valid user can access this.'
        ],
        'put' => [
            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            'security_message' => 'Only admin/app can add new resource to this entity type.'
        ],
        'patch' => [
            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            'security_message' => 'Only admin/app can add new resource to this entity type.'
        ],
        'delete' => [
            'security' => 'is_granted("ROLE_APLIKASI") or is_granted("ROLE_ADMIN") or is_granted("ROLE_UPK_PUSAT")',
            'security_message' => 'Only admin/app can add new resource to this entity type.'
        ],
    ],
    attributes: [
        'security' => 'is_granted("ROLE_USER")',
        'security_message' => 'Only a valid user can access this.',
    ],
    denormalizationContext: [
        'groups' => ['workflow:write'],
        'swagger_definition_name' => 'write'
    ],
    normalizationContext: [
        'groups' => ['workflow:read'],
        'swagger_definition_name' => 'read'
    ]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'uuid' => 'exact',
        'unitId' => 'exact',
        'nextUnitId' => 'exact',
        'officeId' => 'exact',
        'nextOfficeId' => 'exact',
        'employeeId' => 'exact',
        'nextEmployeeId' => 'exact',
        'ticketNumber' => 'ipartial',
        'unitName' => 'ipartial',
        'nextUnitName' => 'ipartial',
        'officeName' => 'ipartial',
        'nextOfficeName' => 'ipartial',
        'employeeName' => 'ipartial',
        'nextEmployeeName' => 'ipartial',
        'employeeNip18' => 'exact',
        'nextEmployeeNip18' => 'exact',
        'currentCase' => 'iexact',
        'nextCase' => 'iexact',
        'caseOutput' => 'iexact',
        'description' => 'ipartial',
        'applicationName' => 'iexact',
        'url' => 'iexact'
    ]
)]
#[ApiFilter(
    DateFilter::class,
    properties: [
        'createdDate'
    ]
)]
#[ApiFilter(
    NumericFilter::class,
    properties: [
        'caseOrder'
    ]
)]
class WorkflowCase
{
    #[MongoDB\Id(
        strategy: 'auto'
    )]
    #[ApiProperty(
        identifier: false
    )]
    private $internalId;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[MongoDB\UniqueIndex(
        name: 'idx_workflow_cases_unique_uuid'
    )]
    #[MongoDB\Index(
        name: 'idx_workflow_cases_uuid'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[ApiProperty(
        identifier: true
    )]
    private $uuid;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[MongoDB\Index(
        name: 'idx_workflow_cases_ticket_number',
        order: 'ASC'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $ticketNumber;

    #[MongoDB\Field(
        type: 'string',
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $unitId;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $unitName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $nextUnitId;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $nextUnitName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $officeId;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $officeName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $nextOfficeId;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $nextOfficeName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[MongoDB\Index(
        name: 'idx_workflow_cases_employee_id'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $employeeId;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $employeeNip18;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $employeeName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $nextEmployeeId;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $nextEmployeeNip18;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $nextEmployeeName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $currentCase;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $nextCase;

    #[MongoDB\Field(
        type: 'int'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $caseOrder;

    #[MongoDB\Field(
        type: 'date_immutable'
    )]
    #[MongoDB\Index(
        name: 'idx_workflow_cases_created_date'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $createdDate;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $caseOutput;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $description;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    #[NotBlank]
    private $applicationName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $businessProcessName;

    #[MongoDB\Field(
        type: 'string'
    )]
    #[Groups([
        'workflow:write',
        'workflow:read'
    ])]
    private $url;

    /**
     * @return mixed
     */
    public function getInternalId()
    {
        return $this->internalId;
    }

    /**
     * @param mixed $internalId
     */
    public function setInternalId($internalId): void
    {
        $this->internalId = $internalId;
    }

    /**
     * @return mixed
     */
    public function getTicketNumber()
    {
        return $this->ticketNumber;
    }

    /**
     * @param mixed $ticketNumber
     */
    public function setTicketNumber($ticketNumber): void
    {
        $this->ticketNumber = $ticketNumber;
    }

    /**
     * @return mixed
     */
    public function getUnitId()
    {
        return $this->unitId;
    }

    /**
     * @param mixed $unitId
     */
    public function setUnitId($unitId): void
    {
        $this->unitId = $unitId;
    }

    /**
     * @return mixed
     */
    public function getUnitName()
    {
        return $this->unitName;
    }

    /**
     * @param mixed $unitName
     */
    public function setUnitName($unitName): void
    {
        $this->unitName = $unitName;
    }

    /**
     * @return mixed
     */
    public function getNextUnitId()
    {
        return $this->nextUnitId;
    }

    /**
     * @param mixed $nextUnitId
     */
    public function setNextUnitId($nextUnitId): void
    {
        $this->nextUnitId = $nextUnitId;
    }

    /**
     * @return mixed
     */
    public function getNextUnitName()
    {
        return $this->nextUnitName;
    }

    /**
     * @param mixed $nextUnitName
     */
    public function setNextUnitName($nextUnitName): void
    {
        $this->nextUnitName = $nextUnitName;
    }

    /**
     * @return mixed
     */
    public function getOfficeId()
    {
        return $this->officeId;
    }

    /**
     * @param mixed $officeId
     */
    public function setOfficeId($officeId): void
    {
        $this->officeId = $officeId;
    }

    /**
     * @return mixed
     */
    public function getOfficeName()
    {
        return $this->officeName;
    }

    /**
     * @param mixed $officeName
     */
    public function setOfficeName($officeName): void
    {
        $this->officeName = $officeName;
    }

    /**
     * @return mixed
     */
    public function getNextOfficeId()
    {
        return $this->nextOfficeId;
    }

    /**
     * @param mixed $nextOfficeId
     */
    public function setNextOfficeId($nextOfficeId): void
    {
        $this->nextOfficeId = $nextOfficeId;
    }

    /**
     * @return mixed
     */
    public function getNextOfficeName()
    {
        return $this->nextOfficeName;
    }

    /**
     * @param mixed $nextOfficeName
     */
    public function setNextOfficeName($nextOfficeName): void
    {
        $this->nextOfficeName = $nextOfficeName;
    }

    /**
     * @return mixed
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @param mixed $employeeId
     */
    public function setEmployeeId($employeeId): void
    {
        $this->employeeId = $employeeId;
    }

    /**
     * @return mixed
     */
    public function getEmployeeNip18()
    {
        return $this->employeeNip18;
    }

    /**
     * @param mixed $employeeNip18
     */
    public function setEmployeeNip18($employeeNip18): void
    {
        $this->employeeNip18 = $employeeNip18;
    }

    /**
     * @return mixed
     */
    public function getEmployeeName()
    {
        return $this->employeeName;
    }

    /**
     * @param mixed $employeeName
     */
    public function setEmployeeName($employeeName): void
    {
        $this->employeeName = $employeeName;
    }

    /**
     * @return mixed
     */
    public function getNextEmployeeId()
    {
        return $this->nextEmployeeId;
    }

    /**
     * @param mixed $nextEmployeeId
     */
    public function setNextEmployeeId($nextEmployeeId): void
    {
        $this->nextEmployeeId = $nextEmployeeId;
    }

    /**
     * @return mixed
     */
    public function getNextEmployeeNip18()
    {
        return $this->nextEmployeeNip18;
    }

    /**
     * @param mixed $nextEmployeeNip18
     */
    public function setNextEmployeeNip18($nextEmployeeNip18): void
    {
        $this->nextEmployeeNip18 = $nextEmployeeNip18;
    }

    /**
     * @return mixed
     */
    public function getNextEmployeeName()
    {
        return $this->nextEmployeeName;
    }

    /**
     * @param mixed $nextEmployeeName
     */
    public function setNextEmployeeName($nextEmployeeName): void
    {
        $this->nextEmployeeName = $nextEmployeeName;
    }

    /**
     * @return mixed
     */
    public function getCurrentCase()
    {
        return $this->currentCase;
    }

    /**
     * @param mixed $currentCase
     */
    public function setCurrentCase($currentCase): void
    {
        $this->currentCase = $currentCase;
    }

    /**
     * @return mixed
     */
    public function getNextCase()
    {
        return $this->nextCase;
    }

    /**
     * @param mixed $nextCase
     */
    public function setNextCase($nextCase): void
    {
        $this->nextCase = $nextCase;
    }

    /**
     * @return mixed
     */
    public function getCaseOrder()
    {
        return $this->caseOrder;
    }

    /**
     * @param mixed $caseOrder
     */
    public function setCaseOrder($caseOrder): void
    {
        $this->caseOrder = $caseOrder;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    public function setCreatedDate($createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getCaseOutput()
    {
        return $this->caseOutput;
    }

    /**
     * @param mixed $caseOutput
     */
    public function setCaseOutput($caseOutput): void
    {
        $this->caseOutput = $caseOutput;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getApplicationName()
    {
        return $this->applicationName;
    }

    /**
     * @param mixed $applicationName
     */
    public function setApplicationName($applicationName): void
    {
        $this->applicationName = $applicationName;
    }

    /**
     * @return mixed
     */
    public function getBusinessProcessName()
    {
        return $this->businessProcessName;
    }

    /**
     * @param mixed $businessProcessName
     */
    public function setBusinessProcessName($businessProcessName): void
    {
        $this->businessProcessName = $businessProcessName;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

}

