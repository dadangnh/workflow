<?php

namespace App\Controller;

use App\Document\WorkflowCase;
use DateTime;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WorkflowCaseCustomController extends AbstractController
{
    private ManagerRegistry $odm;

    public function __construct(ManagerRegistry $odm)
    {
        $this->odm = $odm;
    }

    /**
     * Method to get the cases on specific date range
     * @throws JsonException
     * @throws Exception
     */
    #[Route('/workflow_cases/case_output', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function showCasesByDateRange(Request $request): JsonResponse
    {
        // Make sure that the user is logged in
        if (!$this->isGranted('ROLE_USER')) {
            return $this->json([
                'code' => 401,
                'error' => 'Unauthorized API access.',
            ], 401);
        }

        // Make sure there is content on request
        if (empty($request->getContent())) {
            return $this->json([
                'code' => 422,
                'error' => 'Cannot process empty request.',
            ], 422);
        }

        // Decode the request content
        $content = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (empty($content['start_date'])) {
            return $this->json([
                'code' => 422,
                'error' => 'Please provide start_date parameter.',
            ], 422);
        }

        if (empty($content['end_date'])) {
            return $this->json([
                'code' => 422,
                'error' => 'Please provide end_date parameter.',
            ], 422);
        }

        $startDate = $content['start_date'];
        $endDate = $content['end_date'];

        // Make sure only valid date permitted
        if (!$this->validateDate($startDate) || !$this->validateDate($endDate)) {
            return $this->json([
                'code' => 422,
                'error' => 'Please provide a valid date as the start_date and end_date parameter value.',
            ], 422);
        }

        // Get the data
        $workflowCases = $this->odm
            ->getRepository(WorkflowCase::class)
            ->findWorkflowCaseFromDateRange($startDate, $endDate);

        // Process the value as format
        return $this->json($this->processEndpointOutput($workflowCases));
    }

    /**
     * @param $workflowCases
     * @return array
     */
    #[ArrayShape(['code' => "int", 'message' => ""])]
    private function processEndpointOutput($workflowCases): array
    {
        return [
            'code' => 200,
            'message' => $workflowCases
        ];
    }

    /**
     * @param $date
     * @return bool
     */
    private function validateDate($date): bool
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }
}
