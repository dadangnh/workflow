<?php

namespace App\Repository;

use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

class WorkflowCaseRepository extends DocumentRepository
{
    /**
     * @throws MongoDBException
     */
    public function findWorkflowCaseFromDateRange($startDate, $endDate)
    {
        $query = $this->createQueryBuilder();
        $query->field('createdDate')->gte($startDate);
        $query->field('createdDate')->lte($endDate);
        return $query->getQuery()->execute();
    }
}
